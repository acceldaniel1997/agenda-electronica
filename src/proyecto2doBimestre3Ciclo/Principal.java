//paquete donde se guarda la clase
package proyecto2doBimestre3Ciclo;

//paquetes que se han importado
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Formatter;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

//clase principal de mi proyecto
public class Principal {

    //metodo principal 
    public static void main(String[] args) {

        //objetos
        Agenda a = new Agenda();
        Scanner teclado = new Scanner(System.in);
        //variables 
        boolean salir = false;
        int opcion = 0;
        try {//inspecciona que no haya excepciones
            Scanner agendaSalida = new Scanner(new File("miAgenda.csv"));
            Formatter agendaEntrada = new Formatter(new FileOutputStream("miAgenda.csv",true));
            //condicion para terminar el proceso de ejecucion
            while (!salir) {

                //menu
                System.out.println("1. Añadir contacto");
                System.out.println("2. Imprimir agenda");
                System.out.println("3. Buscar contacto");
                System.out.println("4. Existe contacto");
                System.out.println("5. Salir");
                

                System.out.println("Escribe una de las opciones");
                opcion = teclado.nextInt();//ingresamos una opcion por teclado

                switch (opcion) {//opciones que tiene nuestro menu
                    case 1:

                        //Pido valores
                        System.out.println("Escribe un nombre");
                        String nombre = teclado.next();

                        System.out.println("Escribe un apellido");
                        String apellido = teclado.next();

                        System.out.println("Escribe un telefono");
                        String telefono = teclado.next();

                        //Creo el contacto
                        Contacto c = new Contacto(nombre, apellido, telefono);

                        //se agrega el contacto en el csv
                        a.agregarContacto(c,agendaEntrada);
                        
                        System.out.println("***************************");

                        break;
                    case 2:

                        //imprimo los contactos por pantalla
                        a.imprimirAgenda(agendaSalida);
                        agendaSalida.close();
                        System.out.println("***************************");

                        break;
                    case 3:

                        //pido el nombre
                        System.out.println("Escribe un nombre");
                        nombre = teclado.next();

                        //busca el contacto por su nombre en el csv
                        a.buscarPorNombre(nombre,agendaSalida);
                        agendaSalida.close();
                        System.out.println("***************************");

                        break;
                    case 4:

                        //pido el nombre
                        System.out.println("Escribe un nombre");
                        nombre = teclado.next();

                        //si se cumple la condicion existe un contacto
                        if (a.existeContacto(nombre,agendaSalida)) {
                            System.out.println("Existe contacto");
                        } else {//si no se cumple la condicion no existe contacto
                            System.out.println("No existe contacto");
                        }
                        agendaSalida.close();
                        System.out.println("***************************");

                        break;

                    case 5:

                        //variable para salir de la ejecucion
                        agendaEntrada.close();
                        agendaSalida.close();
                        salir = true;

                        break;

                    default:
                        //si no se ingresa un numero correcto 
                        System.out.println("***************************");
                        System.out.println("Solo números entre 1 y 8");
                        teclado.next();//volver a ingresar un numero

                }

            }
        } catch (InputMismatchException e) {//excepcion tipo de dato mal ingresado
            System.out.println("***************************");
            System.out.println("Debes insertar un número");
            teclado.next();//volver a ingresar un numero

        } catch (FileNotFoundException e) { //excepcion de si no se encuentra el archivo
            System.out.println("No se encuentra el archivo");//mensaje de que no se encuentra el archivo
        }

    }

}
