//paquete donde se guarda la clase
package proyecto2doBimestre3Ciclo;

//clase para introducir los parametros del contacto
public class Contacto {

    //parametros de la clase
    private String nombre, apellido;
    private String telefono;
    
    //constructor con parametros
    public Contacto(String nombre, String apellido, String telefono) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
    }
    
    //getters and setters de los parametros
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

}
