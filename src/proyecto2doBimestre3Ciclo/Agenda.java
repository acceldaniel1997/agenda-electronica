//paquete donde se guarda la clase
package proyecto2doBimestre3Ciclo;

//paquetes que se han importado

import java.util.Formatter;
import java.util.Scanner;



//clase agenda donde van a estar los metodos a usar
public class Agenda {
    
    //metodo para agregar un contacto la agenda
    /**
     * metodo para agregar un contacto la agenda
     *
     * @param c
     * @param agendaEntrada
     */
    public void agregarContacto(Contacto c, Formatter agendaEntrada) {

            //formato del archivo
            agendaEntrada.format("%s;%s;%s;", c.getNombre(), c.getApellido(), c.getTelefono());
            agendaEntrada.format("\r\n");
            
    }
    /**
     * metodo para imprimir la lista
     *
     * @param agendaSalida
     */
    public void imprimirAgenda(Scanner agendaSalida) {

            //condicion para que las lineas escritas en el cvs
            while (agendaSalida.hasNext()) {
                //separa cada linea con ; y se hace un arreglo con el numero de separaciones
                String[] contacto = agendaSalida.nextLine().split(";");
                //impreme cada contacto
                System.out.println("Nombre: " + contacto[0] + "\nApellido: " + contacto[1] + "\nTelefono: " + contacto[2]);
                System.out.println("***************************");

            }
    }
    /**
     * metodo para saber si existe el contacto en la lista 
     *
     * @param nombre
     * @param agendaSalida
     * @return 
     */
    public boolean existeContacto(String nombre,Scanner agendaSalida) {

            //condicion para que las lineas escritas en el cvs
            while (agendaSalida.hasNext()) {
                //separa cada linea con ; y se hace un arreglo con el numero de separaciones
                String[] contacto = agendaSalida.nextLine().split(";");
                //condicion que iguala el contacto con el nombre ingresado 
                if (contacto[0] != null &&contacto[0].equalsIgnoreCase(nombre)) {
                    
                    return true;//si se encuentra el contacto devuelve true
                }
                
            }
            return false;//si no se encuentra el contacto devuelve false
    }

    /**
     * metodo que busca un contacto por su nombre
     *
     * @param nombre
     * @param agendaSalida
     */
    public void buscarPorNombre(String nombre,Scanner agendaSalida) {
        //variable booleana que nos sirve para saber si se encontro el contacto
        boolean encontrado = false;

            //condicion para que las lineas escritas en el cvs
            while (agendaSalida.hasNext()) {
                //separa cada linea con ; y se hace un arreglo con el numero de separaciones
                String[] contacto = agendaSalida.nextLine().split(";");
                //condicion que iguala el contacto con el nombre ingresado 
                if (contacto[0] != null && contacto[0].equalsIgnoreCase(nombre)) {
                    //devuelve el telefono del contacto
                    System.out.println("Su telefono es: " + contacto[2]);
                    System.out.println("***************************");
                    encontrado = true;//si se encuentra el contacto devuelve true
                }
                else {
                    if (!encontrado) {//condicion si no se encuentra el contacto
                    //mensaje que no se ha encontrado el contacto
                    System.out.println("No se ha encontrado al contacto");
                    System.out.println("***************************");
                }
                    
                }
            }
    }


}

